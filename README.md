# Java Microservice Example

Customer microservice providing RESTful API for customer data. Built with Spring Boot and Docker.

## Build the project and run (without Docker)

- `./gradlew clean build && java -jar build/libs/customer-service-0.1.0.jar`

Tests are run automatically during the build step or via `./gradlew test`

## Build the project and run (with Docker)

### Build docker container and push image

- Make `push = true` within build.gradle
- `eval "$(docker-machine env default)"` if shell not already configured
- `./gradlew clean build buildDocker`

### Run the container

- Run the container `docker run -p 8080:8080 -t jackhoy/customer-service`
- Visit the DOCKER_HOST (the public facing IP of the VM) which is shown when running `docker-machine env`

## Update gradle version

- Update gradle version in wrapper within build.gradle
- `gradle wrapper`
