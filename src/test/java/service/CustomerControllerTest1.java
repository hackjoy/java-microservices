// package service;
//
// com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.*
// com.jayway.restassured.matcher.RestAssuredMatchers.*
// org.hamcrest.Matchers.*
//
// import org.junit.Before;
// import org.junit.Test;
// import org.junit.runner.RunWith;
// import org.springframework.boot.test.SpringApplicationConfiguration;
// import org.springframework.http.MediaType;
// import org.springframework.mock.web.MockServletContext;
// import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import org.springframework.test.context.web.WebAppConfiguration;
// import org.springframework.test.web.servlet.MockMvc;
// import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
// import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
// @RunWith(SpringJUnit4ClassRunner.class)
// @SpringApplicationConfiguration(classes = MockServletContext.class)
// @WebAppConfiguration
// public class CustomerControllerTest {
//
// 	private MockMvc mvc;
//
// 	@Before
// 	public void setUp() throws Exception {
// 		mvc = MockMvcBuilders.standaloneSetup(new CustomerController()).build();
// 	}
//
// 	@Test
// 	public void getIndex() throws Exception {
// 		get("/customers").then().body("customers.count", equalTo(10));
// 	}
// }
