package unit.utils;

import org.json.JSONObject;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import customerService.utils.Formatter;

public class FormatterTest {

    private Formatter formatter;

    @Before
    public void setUp() {
        formatter = new Formatter();
    }

    @Test
    public void getFullName() {
        JSONObject customer = new JSONObject();
        customer.put("title", "Mr");
        customer.put("firstName", "John");
        customer.put("lastName", "Smith");

        assertEquals(
            "should concat title, first name and last name",
            "Mr John Smith",
            formatter.getFullName(customer)
        );
    }
}
