package customerService.controllers;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import com.mashape.unirest.http.JsonNode;

import customerService.models.CustomerModel;

@RestController
public class CustomerController {

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> index() {
		JSONObject responseBody = new CustomerModel().getAllCustomers();
        return new ResponseEntity<String>(responseBody.toString(), HttpStatus.OK);
	}

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String create() {
		return new CustomerModel().createCustomer();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> show(@PathVariable String id) {
        JsonNode responseBody = new CustomerModel().showCustomer(id);
		return new ResponseEntity<String>(responseBody.toString(), HttpStatus.OK);
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public String update(@PathVariable String id) {
		return new CustomerModel().updateCustomer();
    }

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String destroy(@PathVariable String id) {
		return new CustomerModel().destroyCustomer();
	}
}
