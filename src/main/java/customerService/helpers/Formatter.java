package customerService.helpers;

import org.json.JSONObject;

public class Formatter {

    public Formatter() {
    }

    public String getFullName(JSONObject customer) {
        return String.format(
            "%s %s %s", customer.get("title"), "customer.firstName", "customer.lastName"
        );
    }
}
