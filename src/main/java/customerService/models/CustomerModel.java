package customerService.models;

import org.json.*;
import com.mashape.unirest.http.JsonNode;

import customerService.requests.CustomerRequest;

public class CustomerModel {

    public JSONObject getAllCustomers() {
        JSONObject customerObject = new JSONObject();
        // Enrich, transform data etc etc..
        JSONArray dataSourceA = new CustomerRequest().index();
        customerObject.put("customers", dataSourceA);
        customerObject.put("count", dataSourceA.length());

        return customerObject;
    }

    public String createCustomer() {
        return "Created customer A";
    }

    public JsonNode showCustomer(String id) {
        JsonNode json = new CustomerRequest().show(id);
        // Enrich, transform data etc etc..
        return json;
    }

    public String updateCustomer() {
        return "Updated customer A";
    }

    public String destroyCustomer() {
        return "Destroyed customer A";
    }
}
