package customerService.utils;

import org.json.JSONObject;

public class Formatter {

    public String getFullName(JSONObject customer) {
        return String.format(
            "%s %s %s", customer.get("title"), customer.get("firstName"), customer.get("lastName")
        );
    }
}
