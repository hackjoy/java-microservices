package customerService.requests;

import org.json.*;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class CustomerRequest {

    private static String baseUrl = "http://jsonplaceholder.typicode.com/users";

    public JSONArray index() {
        try {
            HttpResponse<JsonNode> response = Unirest.get(baseUrl).header("accept", "application/json").asJson();
            return response.getBody().getArray();
        } catch (UnirestException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JsonNode show(String id) {
        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.get(baseUrl + "/" + id).header("accept", "application/json").asJson();
            return jsonResponse.getBody();
        } catch (UnirestException e) {
            e.printStackTrace();
            return null;
        }
    }
}
